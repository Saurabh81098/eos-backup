## Website Design elements here

All files related to the design of the website of the EOS Design System should be placed inside this folder, properly organized in subfolders.

Example of elements that should be in this folder:
- Landing: files with the design of the EOS presentation website
- Assets: files of the EOS logo, music, video files, etc
- Pages: files with the design of pages in the EOS website, organized in folders
