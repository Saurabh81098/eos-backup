## Design System elements here

All files related to the design of the elements of the Design System should be placed inside this folder, properly organized in subfolders.

Example of elements that should be in this folder:
- Components design: login form for all SUSE products
- Templates design: template for the login page of SUSE products
